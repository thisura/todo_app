angular.
module('ui', [])
  .directive('actionMenu', actionMenu)
  .controller('pages', pages);

function actionMenu() {

  var directive = {
    restrict: 'E',
    template: '<div class="circle_menu"><i class="fa fa-plus menu_btn"></i></div><div class="menu"><i class="fa {{page.icon}} menu_itm" ng-repeat="page in vm.pages" ng-click="vm.open(page.tpl)"></i></div>',
    link: link
  };

  return directive;

  function link(scope, element, attrs) {
    element.on('click', toggle);

    function toggle() {
      element.toggleClass('open');
    }
  }

}

function pages() {
  var vm = this;

  activate();

  function activate() {
    vm.open = open;
    vm.pages = [{
      'tpl': 'Home',
      'icon': 'fa-home'
    }, {
      'tpl': 'Edit',
      'icon': 'fa-pencil'
    }, {
      'tpl': 'Search',
      'icon': 'fa-search'
    }];
    vm.active = vm.pages[0].tpl;
  }

  function open(page) {
    vm.active = page;
  }
}