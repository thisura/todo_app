$(document).ready(function() {

  /**
   * Animating exit button
   */

  //Mouseover for menu choices
  $("#home").mouseover(function() {
    TweenMax.to(this, 0.3, {
      boxShadow: "5px 5px 15px 0px rgba(50, 50, 50, 0.79)",
      zIndex: "8"
    });
  }).mouseout(function() {
    TweenMax.to(this, 0.3, {
      boxShadow: "none",
      zIndex: "7"
    });
  });

  $("#about").mouseover(function() {
    TweenMax.to(this, 0.3, {
      boxShadow: "5px 5px 15px 0px rgba(50, 50, 50, 0.79)",
      zIndex: "8"
    });
  }).mouseout(function() {
    TweenMax.to(this, 0.3, {
      boxShadow: "none",
      zIndex: "7"
    });
  });

  $("#work").mouseover(function() {
    TweenMax.to(this, 0.3, {
      boxShadow: "5px 5px 15px 0px rgba(50, 50, 50, 0.79)",
      zIndex: "8"
    });
  }).mouseout(function() {
    TweenMax.to(this, 0.3, {
      boxShadow: "none",
      zIndex: "7"
    });
  });

  $("#material").mouseover(function() {
    TweenMax.to(this, 0.3, {
      boxShadow: "5px 5px 15px 0px rgba(50, 50, 50, 0.79)",
      zIndex: "8"
    });
  }).mouseout(function() {
    TweenMax.to(this, 0.3, {
      boxShadow: "none",
      zIndex: "7"
    });
  });

  $("#contact").mouseover(function() {
    TweenMax.to(this, 0.3, {
      boxShadow: "5px 5px 15px 0px rgba(50, 50, 50, 0.79)",
      zIndex: "8"
    });
  }).mouseout(function() {
    TweenMax.to(this, 0.3, {
      boxShadow: "none",
      zIndex: "7"
    });
  });

});

/**
 * [index how many times is button clicked]
 * @type {int}
 */
var index = 0;

function getMenu() {
  index += 1;
  var menu = $('#overlay');

  //CLOSING THE MENU
  if ((index % 2) == 0) {
    TweenMax.to(menu, 0.3, {
      delay: 0.8,
      display: "none",
      left: "343px",
      width: "0px",
      height: "0px",
      ease: "Power1.easeInOut"
    });
    //menu button transform
    TweenMax.to($("#menuButton"), 1, {
      backgroundColor: "#ff4081"
    });
    TweenMax.to($("#line1"), 0.3, {
      delay: 0.8,
      rotation: "0",
      height: "4px",
      width: "25px",
      top: "19px",
      left: "17px"
    });
    TweenMax.to($("#line2"), 0.3, {
      delay: 0.8,
      rotation: "0",
      height: "4px",
      width: "25px",
      left: "17px"
    });
    TweenMax.to($("#line3"), 0.3, {
      delay: 0.8,
      rotation: "0",
      height: "4px",
      width: "25px",
      top: "37px",
      left: "17px"
    });
    TweenMax.to($("#line1, #line2, #line3"), 1, {
      backgroundColor: "#fce4ec"
    });

    //choices
    TweenMax.to($("#home"), 0.3, {
      delay: 0.2,
      width: "0px",
      height: "50px",
      display: "none",
      top: "0",
      left: "0",
      fontSize: "0"
    });
    TweenMax.to($("#about"), 0.3, {
      delay: 0.3,
      width: "0px",
      height: "50px",
      display: "none",
      top: "0",
      left: "0",
      fontSize: "0"
    });
    TweenMax.to($("#work"), 0.3, {
      delay: 0.4,
      width: "0px",
      height: "50px",
      display: "none",
      top: "0",
      left: "0",
      fontSize: "0"
    });
    TweenMax.to($("#material"), 0.3, {
      delay: 0.5,
      width: "0px",
      height: "50px",
      display: "none",
      top: "0",
      left: "0",
      fontSize: "0"
    });
    TweenMax.to($("#contact"), 0.3, {
      delay: 0.6,
      width: "0px",
      height: "50px",
      display: "none",
      top: "0",
      left: "0",
      fontSize: "0"
    });

    //Menu bar
    TweenMax.to($("#menuBar"), 0.5, {
      display: "none",
      width: "0px"
    });

    //Search icon
    TweenMax.to($("#searchIcon"), 0.3, {
      display: "none",
      left: "0px"
    });
    searchClick = 0;
    TweenMax.to($("#magnifierIcon"), 0.3, {
      rotation: 0,
      backgroundImage: "url(http://s30.postimg.org/5vghezzkd/search_white.png)"
    });
    //Search bar

    TweenMax.to($("#searchBar"), 0.3, {
      width: 0,
      paddingLeft: 0
    });
  }
  //OPENING THE MENU
  else if ((index % 2) !== 0) {
    TweenMax.to(menu, 0.3, {
      display: "initial",
      left: "18%",
      width: "1200px",
      height: "1250px",
      ease: "Power1.easeInOut"
    });
    //menu button transform 
    TweenMax.to($("#menuButton"), 1, {
      backgroundColor: "#fce4ec"
    });
    TweenMax.to($("#line1"), 0.3, {
      delay: 0.2,
      rotation: "145_short",
      backgroundColor: "#ff4081",
      height: "2px",
      width: "17px",
      top: "22px",
      left: "14px"
    });
    TweenMax.to($("#line2"), 0.3, {
      delay: 0.2,
      rotation: "-180_short",
      backgroundColor: "#ff4081",
      height: "2px",
      width: "29px",
      left: "14px"
    });
    TweenMax.to($("#line3"), 0.3, {
      delay: 0.2,
      rotation: "-145_short",
      backgroundColor: "#ff4081",
      height: "2px",
      width: "17px",
      top: "34px",
      left: "14px"
    });

    //choices
    TweenMax.to($("#home"), 0.5, {
      delay: 0.3,
      width: "1000px",
      height: "200px",
      display: "initial",
      top: "120px",
      left: "100px",
      fontSize: "48"
    });
    TweenMax.to($("#about"), 0.5, {
      delay: 0.4,
      width: "1000px",
      height: "200px",
      display: "initial",
      top: "321px",
      left: "100px",
      fontSize: "48"
    });
    TweenMax.to($("#work"), 0.5, {
      delay: 0.5,
      width: "1000px",
      height: "200px",
      display: "initial",
      top: "522px",
      left: "100px",
      fontSize: "48"
    });
    TweenMax.to($("#material"), 0.5, {
      delay: 0.6,
      width: "1000px",
      height: "200px",
      display: "initial",
      top: "723px",
      left: "100px",
      fontSize: "48"
    });
    TweenMax.to($("#contact"), 0.5, {
      delay: 0.7,
      width: "1000px",
      height: "200px",
      display: "initial",
      top: "924px",
      left: "100px",
      fontSize: "48"
    });

    //Menu bar
    TweenMax.to($("#menuBar"), 0.5, {
      delay: 0.1,
      display: "initial",
      width: "1200px"
    });
    //Search icon
    TweenMax.to($("#searchIcon"), 0.5, {
      delay: 0.5,
      display: "initial",
      left: "1040px"
    });
    TweenMax.to($("#magnifierIcon"), 0.3, {
      delay: 1,
      rotation: -90
    });

  }
}

$(window).on('scroll', function() {
  var y_scroll_pos = window.pageYOffset;
  var scroll_pos_test = 100; // set to whatever you want it to be

  if (y_scroll_pos > scroll_pos_test) {
    TweenMax.to($("#topPic"), 0.4, {
      height: "500px",
      ease: "Power1.easeIn",
      backgroundPosition: "0 -250px"
    });
    TweenMax.to($("#titleOverlay"), 0.4, {
      height: "500px",
      ease: "Power1.easeIn"
    });
  } else {
    TweenMax.to($("#topPic"), 0.4, {
      height: "750px",
      ease: "Power1.easeOut",
      backgroundPosition: "0 0px"
    });
    TweenMax.to($("#titleOverlay"), 0.4, {
      height: "400px",
      ease: "Power1.easeOut"
    });
  }
});

var searchClick = 0;
//Get the search field
function getSearch() {
  searchClick += 1;
  console.log(searchClick);

  if ((searchClick % 2) !== 0) {
    TweenMax.to($("#searchBar"), 0.5, {
      width: "980px",
      paddingLeft: "20px",
      ease: "Power1.easeInOut"
    });
    TweenMax.to($("#magnifierIcon"), 0.5, {
      rotation: "270",
      backgroundImage: "url(http://s30.postimg.org/ydb51e37x/search.png)"
    });
  } else if ((searchClick % 2) == 0) {
    TweenMax.to($("#searchBar"), 0.5, {
      width: "0px",
      paddingLeft: "0px",
      ease: "Power1.easeInOut"
    });
    TweenMax.to($("#magnifierIcon"), 0.5, {
      rotation: "-90",
      backgroundImage: "url(http://s30.postimg.org/5vghezzkd/search_white.png)"
    });
  }
}