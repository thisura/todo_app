app.controller("Todo", function($scope)
	 { 
		$scope.records = [
			{ID: "P001", Name:"Jerome", Record: "Board meeting", Address: "No,25,Lake drive"},
			{ID: "P002", Name:"Mathew", Record: "Client meeting", Address: "No,23,Eden park" },
            {ID: "P003", Name: "Jason", Record: "Paying monthly bills", Address: "No,18,Eden Gardens" }
		
		];
        $scope.editing = false;
		$scope.addRecord = function(record) {
			$scope.records.push(record);
			$scope.record = {};
		}
		
		/*$scope.totalPrice = function(){
			var total = 0;
			for(count=0;count<$scope.items.length;count++){
				total += $scope.items[count].Price*$scope.items[count].Quantity;
			}
			return total;
		}*/
		
		$scope.removeRecord = function(index){
			$scope.records.splice(index,1);
		}
		$scope.editRecord = function(index){
			 $scope.editing = $scope.records.indexOf(index);
			   
		}
		 $scope.saveField = function(index) {
        if ($scope.editing !== false) {
			$scope.editing = false;
        }  
        

		 $scope.findRecord = function(index)
    { 
        $scope.filteredList  = _.filter($scope.records,
                 function(record){  
                     return searchUtil(record,$scope.searchText); 
                 });
        
        if($scope.searchText == '')
        {
            $scope.filteredList = $scope.records ;
        }
    }  
    function searchUtil(record,toSearch)
{
    /* Search Text in all 3 fields */
    return ( record.Name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || record.Address.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || record.ID == toSearch
                            )                              
                     ? true : false ;
}
             
    };
    
    $scope.cancel = function(index) {
        if ($scope.editing !== false) {
            $scope.editing = false;
        }       
    };
	}
	);